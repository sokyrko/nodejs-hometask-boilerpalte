const { BaseRepository } = require('./baseRepository');

class UserRepository extends BaseRepository {
    constructor() {
        super('users');
    }

    createUsers(payload){
        if(payload){
            super.create(payload)
            return true
        }
        else {
            return false

        }
    }

    updateUsers(id,payload){
        if(id && payload){
            super.update(id, payload)
            return true;
        }
        else {
            return false

        }
    }
    getUsers() {
        if (super.getAll()) {
            return super.getAll()
        } else {
            return false
        }
    }
    getUsersById(id){
        if(super.getOne(id)) {
            return super.getOne(id)
        }
        else {
            return false
        }
    }
    deleteUsers(id){
        if(id){
            super.delete(id)
        } else {
            return false
        }
    }
}

exports.UserRepository = new UserRepository();
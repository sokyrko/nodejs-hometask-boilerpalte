const { BaseRepository } = require('./baseRepository');

class FighterRepository extends BaseRepository {
    constructor() {
        super('fighters');
    }
    createFighters(payload){
        if(payload){
            super.create(payload)
            console.log('Fighter saved')
            return true
        }
        else {
            return false

        }
    }

    updateFighters(id,payload){
        if(id && payload){
            super.update(id, payload)
            return true;
        }
        else {
            return false

        }
    }
    getFighters(){
        if(super.getAll()) {
            return super.getAll()
        }
        else {
            return false
        }
    }
    getFightersById(id){
        // console.log(typeof id)
        if(super.getOne(id)) {
            return super.getOne(id)
        }
        else {
            return false
        }
    }
    deleteFighters(id){
        if(id){
            super.delete(id)
        } else {
            return false
        }
    }
}

exports.FighterRepository = new FighterRepository();
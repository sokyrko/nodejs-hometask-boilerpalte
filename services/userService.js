const { UserRepository } = require('../repositories/userRepository');
const {user} = require('../models/user');

class UserService {

    // TODO: Implement methods to work with user

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }


    //tasks
    // USER:
    // GET /api/users
    // GET /api/users/:id
    // POST /api/users
    // PUT /api/users/:id
    // DELETE /api/users/:id

    getUsers = () => {
        if (UserRepository.getUsers()) {
            const result = UserRepository.getUsers();
            console.log(result)
            if(result) {
                return true;
            } else {
                return false
            }
        } else {
            return null;
        }

    }
    createUsers= (payload) => {
        if (payload) {
            const allUsers = UserRepository.getUsers();
            if(
                allUsers.find((user)=> user.email.toLowerCase()===payload.email.toLowerCase()) ||
                allUsers.find((user)=>user.phoneNumber === payload.phoneNumber)
            ){
                return false
            } else {
                UserRepository.createUsers(payload)
                return true;
            }
        } else {
             return null;
        }
    }
    getUsersById = (params) => {
        if ( params) {
            let result = false
            Object.keys(user).forEach((key) => {
                const src = {}
                src[key]=params.id
                let user = UserRepository.getUsersById(src);
                if (user) result = true;
            })
            // const result = UserRepository.getUsersById({id:params.id});
            if(result){
                return true
            } else {
                return false
            }
        } else {
            return null;
        }
    }
    updateUsers = (userId, payload) => {
        if (payload && userId) {
            const allUsers = UserRepository.getUsers();
            if (
                allUsers.find((user) => user.id!==userId && user.email.toLowerCase() === payload.email.toLowerCase()) ||
                allUsers.find((user) => user.id!==userId && user.phoneNumber === payload.phoneNumber)
            ) {
                return false
            } else {
                UserRepository.updateUsers(userId, payload)
                return true
            }

        } else {
            return null;
        }
    }
    deleteUsers = (id) => {
        if (id) {
            const result = UserRepository.getUsersById({id: id});

            if (result) {
                UserRepository.delete(id)
                return true;
            } else {
                return false;
            }
        } else {
            return null
        }
    }


}

module.exports = new UserService();
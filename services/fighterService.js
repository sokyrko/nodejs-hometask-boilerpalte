const { FighterRepository } = require('../repositories/fighterRepository');
const {fighter} = require('../models/fighter');

class FighterService {
    // TODO: Implement methods to work with fighters+ FIGHTER
    //         GET /api/fighters
    //         GET /api/fighters/:id
    //         POST /api/fighters
    //         PUT /api/fighters/:id
    //         DELETE /api/fighters/:id

    getFighters = () => {
        if (FighterRepository.getFighters()) {
            const result = FighterRepository.getFighters();
            return result;
        } else {
            return null;
        }
    }
    createFighters= (payload) => {
        if (payload) {
            const allFighters = FighterRepository.getFighters();
            if(
                allFighters.find((fighter)=> fighter.name===payload.name)
            ){
                return false
            } else {
                FighterRepository.createFighters(payload)
                return true;
            }
        } else {
            return null;
        }
    }
    getFightersById = (params) => {
        if ( params) {
            let result = false
            Object.keys(fighter).forEach((key) => {
                const src = {}
                src[key]=params.id
                let fighter = FighterRepository.getFightersById(src);
                if (fighter) result = true;
            })
                // result = FighterRepository.getFightersById({id: params.id});

            if(result){
                return true
            } else {
                return false
            }
        } else {
            return null;
        }
    }
    updateFighters = (fighterId, payload) => {
        if (payload && fighterId) {
            const allFighters = FighterRepository.getFighters();
            // const result = FighterRepository.getFightersById({id: fighterId.toLowerCase()});
            if (
                allFighters.find((fighter) => fighter.id!==fighterId
                    && fighter.name.toLowerCase() === payload.name.toLowerCase())
            ) {
                return false
            } else {
                FighterRepository.updateFighters(fighterId, payload)
                return true
            }

        } else {
            return null;
        }
    }
    deleteFighters = (id) => {
        if (id) {
            const result = FighterRepository.getFightersById({id: id});

            if (result) {
                FighterRepository.delete(id)
                return true;
            } else {
                return false;
            }
        } else {
            return null
        }
    }



}

module.exports = new FighterService();
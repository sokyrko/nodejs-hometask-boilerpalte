const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter

//get all fightes
router.get('/',   async function (req, res, next) {
    try {
        const result = FighterService.getFighters()
        if (result || !result) {
            if(result) {
                res.status(200).send({message: 'Fighters are fond'})
            } else  {
                res.status(400).send({message:'Fighters are not fond'})
            }

        } else {
            res.status(404).json({error: true,
                message:  'error'})
        }
    } catch (e) {
        res.status(400).json({error: true,
            message: e.error})
    }

})

//get fighter by Id
router.get('/:id',   async function (req, res, next) {
    try {
        const result = FighterService.getFightersById(req.params)

        if (result || !result) {
            if(result===true){
                res.status(200).send({message:"Fighter success fond"})
            } else {
                res.status(400).send({message:"Fighter doesn't exist"})
            }

        } else {
            res.status(404).json({error: true,
                message: ' error'})
        }
    } catch (e) {
        res.status(400).json({error: true,
            message: e.error})
    }

})
//Create fighter
router.post('/', createFighterValid,  async function (req, res, next) {
    try {
        const result = FighterService.createFighters( req.body)
        if (result || !result) {
            if(result===true){
                res.status(200).send({message:"Fighter success created"})
            } else {
                res.status(400).send({message:"Fighter already exist"})
            }
        } else {
            res.status(404).json({error: true,
                message: ' error'})
        }
    } catch (e) {
        res.status(400).json({error: true,
            message: e.error})
    }


})

//update fighter
router.put('/:id', updateFighterValid,  async function (req, res, next) {
    try {
        const fighterId = req.params.id
        const result =   FighterService.updateFighters( fighterId, req.body)
        if(result || !result) {
            if (result) {
                res.status(200).send({message: "Fighter success updated"})
            } else {
                res.status(400).send({message: "Double names of users"})
            }
        } else {
            res.status(404).json({error: true,
                message: ' error'})
        }
    } catch (e) {
        res.status(400).json({error: true,
            message: e.error})
    }

})
//delete user
router.delete('/:id',  async function (req, res, next) {
    try {
        const userId = req.params.id
        const result = FighterService.deleteFighters( userId)
        if (result || !result) {
            if (result ) {
                res.status(200).send({message: "Fighter is success deleted"})
            } else {
                res.status(400).send({message: "Fighter is not fond"})
            }
        } else {
            res.status(404).json({error: true,
                message: ' error'})
        }
    } catch (e) {
        res.status(400).json({error: true,
            message: e.error})
    }

})
module.exports = router;
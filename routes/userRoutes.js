const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user
// USER:
// GET /api/users
// GET /api/users/:id
// POST /api/users
// PUT /api/users/:id
// DELETE /api/users/:id

//get all users
router.get('/',   async function (req, res, next) {
    try {
        const result = UserService.getUsers()
        if (result || !result) {
            if(result) {
                res.status(200).send({message: 'Users are fond'})
            } else  {
                res.status(400).send({message:'Users are not fond'})
            }

        } else {
            res.status(404).json({error: true,
                message:  'error'})
        }
    } catch (e) {
       res.status(400).json({error: true,
           message: e.error})
    }

})

//get user by Id
router.get('/:id',   async function (req, res, next) {
    try {
        const result = UserService.getUsersById(  req.params)

        if (result || !result) {
            if(result===true){
                res.status(200).send({message:"User success fond"})
            } else {
                res.status(400).send({message:"User doesn't exist"})
            }

        } else {
            res.status(404).json({error: true,
                message: ' error'})
        }
    } catch (e) {
        res.status(400).json({error: true,
            message: e.error})
    }

})

//Create user
router.post('/', createUserValid,  async function (req, res, next) {
    try {
        const result = UserService.createUsers( req.body)
        if (result || !result) {
            if(result===true){
                res.status(200).send({message:"User success created"})
            } else {
                res.status(400).send({message:"User already exist"})
            }
        } else {
            res.status(404).json({error: true,
                message: ' error'})
        }
    } catch (e) {
        res.status(400).json({error: true,
            message: e.error})
    }

})

//update user
router.put('/:id', updateUserValid,  async function (req, res, next) {
    try {
        const userId = req.params.id
        const result =   UserService.updateUsers( userId, req.body)
         if(result || !result) {
            if (result) {
                res.status(200).send({message: "User success updated"})
            } else {
                res.status(400).send({message: "Double email or phone"})
            }
         } else {
             res.status(404).json({error: true,
                 message: ' error'})
         }
    } catch (e) {
        res.status(400).json({error: true,
            message: e.error})
    }

})
//delete user
router.delete('/:id',  async function (req, res, next) {
    try {
        const userId = req.params.id
        const result = UserService.deleteUsers( userId)
        if (result || !result) {
            if (result ) {
                res.status(200).send({message: "User is success deleted"})
            } else {
                res.status(400).send({message: "User is not fond"})
            }
        } else {
            res.status(404).json({error: true,
                message: ' error'})
        }
    } catch (e) {
        res.status(400).json({error: true,
            message: e.error})
    }

})
module.exports = router;
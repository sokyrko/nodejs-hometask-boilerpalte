const { user } = require('../models/user');

const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    let email = /[^.]+@gmail.com/;
    let phone = /^\+380\d{9}$/ig;
    let matchKeys=[];
    let userKeys =[];
    Object.keys(user).forEach((keyModel)=>{
          Object.keys(req.body).filter((keyForm)=> {
             if( keyForm === keyModel ) matchKeys.push(keyForm)
          })
          if(keyModel !== "id") userKeys.push(keyModel)
    })
    if (matchKeys.length === userKeys.length &&
        (
            req.body.email.trim().match(email) &&
            req.body.phoneNumber.trim().match(phone) &&
            req.body.firstName.trim().length >= 3 &&
            req.body.lastName.trim().length >= 3 &&
            req.body.password.trim().length >= 3
        )

    ) {
    next();
    } else {
        res.status(400).send({message:"Error create user"})
    }
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    let email = /[^.]+@gmail.com$/;
    let phone = /^\+380\d{9}$/ig;
    let matchKeys=[];
    let userKeys =[];
    Object.keys(user).forEach((keyModel)=>{
        Object.keys(req.body).filter((keyForm)=> {
            if( keyForm === keyModel ) matchKeys.push(keyForm)
        })
        if(keyModel !== "id") userKeys.push(keyModel)
    })
    if ( (matchKeys.length === userKeys.length
        && req.body.email.trim().match(email)
        &&  req.body.phoneNumber.trim().match(phone)
        ) &&
       ( req.body.firstName.trim().length >= 3 ||
        req.body.lastName.trim().length >= 3 ||
        req.body.password.trim().length>=3)
    ) {
        next();
    } else {
        res.status(400).send({message:"Error create user"})
    }
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
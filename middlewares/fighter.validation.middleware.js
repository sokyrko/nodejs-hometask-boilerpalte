const { fighter } = require('../models/fighter');
//      "name": "",
//     "power": 0,
//     "defense": 1, // 1 to 10

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    let matchKeys=[];
    let fighterKeys =[];
    Object.keys(fighter).forEach((keyModel)=>{
        Object.keys(req.body).filter((keyForm)=> {
            if( keyForm === keyModel ) matchKeys.push(keyForm)
        })
        if(keyModel !== "id"  &&  keyModel !== "health" ) fighterKeys.push(keyModel)
    })
    if (matchKeys.length === fighterKeys.length &&
        ( req.body.name.trim().length >= 3  &&
            ( req.body.power < 100 && req.body.power > 1 ) &&
            (req.body.defense < 10 && req.body.defense > 1 )
        )
    ) {
        next();
    } else {
        res.status(400).send({message:"Error create fighter"})
    }
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    let matchKeys=[];
    let fighterKeys =[];
    Object.keys(fighter).forEach((keyModel)=>{
        Object.keys(req.body).filter((keyForm)=> {
            if( keyForm === keyModel  ) matchKeys.push(keyForm)
        })
        if(keyModel !== "id"  &&  keyModel !== "health" ) fighterKeys.push(keyModel)
    })

    if (
        matchKeys.length=== fighterKeys.length &&
                (req.body.power < 100 && req.body.power > 1 ) &&
                (req.body.defense < 10 && req.body.defense > 1)||
                ( typeof req.body.power === "undefined" ) || (typeof req.body.defense === "undefined")
    ) {
        next();
    } else {
        res.status(400).send({message: "Error update fighter"})
    }
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;